# Users Front End - Prueba para Intergrupo e Ilumno

Hola, bienvenido a la prueba,
Este repositorio contiene el proyecto front end

## Version de Angular usada
1.7.2

## Instrucciones para ejecutar

Antes de empezar, desde la carpeta del proyecto, usa el comando `npm install` para instalar todas la dependencias del proyecto, luego corre `npm start` o genera un bundle de la aplicacion con `npm run build` y ponlo en cualquier servidor apache, asegurate de tener el package manager de NPM.

Si ejecutas `npm start`, abre tu navegador en la direccion `http://localhost:8080/`
Y eso es todo :)

