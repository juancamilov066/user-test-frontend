routes.$inject = ['$stateProvider'];

export default function routes($stateProvider){
    $stateProvider
        .state('DeleteUserModule', {
            url: '/delete-user',
            controller: 'DeleteUserController',
            template: require('./delete-user.html')
        })
}