import angular from 'angular';
import uirouter from 'angular-ui-router'
import routes from './delete-user.config';
import UsersService from '../services/users.service';

var deleteUserModule = angular
    .module('UsersApp.DeleteUserModule', [uirouter])
    .service('UsersService', ['$http', UsersService])
    .config(routes)
    .controller('DeleteUserController', ['$scope', 'UsersService', function($scope, UsersService){

        $scope.email = '';
        $scope.error = undefined;
        $scope.success = undefined;

        $scope.deleteUser = function() {
            restart();
            UsersService.deleteUser($scope.email)
                .then(response => {
                    $scope.success = 'Usuario borrado con exito';
                })
                .catch(error => {
                    if (error.status === 404) {
                        $scope.error = 'No encontramos ningun usuario';
                        return;
                    }
                    if (error.status === 500) {
                        $scope.error = 'Error interno, intenta luego';
                        return;
                    }
                });
        }
        
        function restart(){
            $scope.error = undefined;
            $scope.success = undefined;
        }
    }]);

export default deleteUserModule.name;
