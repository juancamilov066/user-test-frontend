import angular from 'angular';
import uirouter from 'angular-ui-router';
import routes from './create-user.config';
import UsersService from '../services/users.service';

var createUserModule = angular
    .module('UsersApp.CreateUserModule', [uirouter])
    .config(routes)
    .service('UsersService', ['$http', UsersService])
    .controller('CreateUserController', ['$scope', '$state', 'UsersService', function($scope, $state, UsersService){

        $scope.email = '';
        $scope.error = undefined;
        $scope.success = undefined;
        $scope.user = {
            email: '',
            phone: '',
            name: '',
            password: ''
        }

        $scope.create = function(){
            restart();
            UsersService.createUser($scope.user)
                .then(response => {
                    $scope.success = 'Usuario creado con exito'
                    created();
                })
                .catch(error => {
                    if (error.status === 409){
                        $scope.error = 'El usuario que intentas crear ya existe'
                    }
                    if (error.status === 500){
                        $scope.error = 'Error interno del servidor'
                    }
                })
        }

        function created(){
            $scope.user = {
                email: '',
                phone: '',
                name: '',
                password: ''
            }
        }

        function restart(){
            $scope.error = undefined;
            $scope.success = undefined;
        }

    }]);

export default createUserModule.name;