routes.$inject = ['$stateProvider'];

export default function routes($stateProvider){
    $stateProvider
        .state('CreateUserModule', {
            url: '/create-user',
            controller: 'CreateUserController',
            template: require('./create-user.html')
        });
}