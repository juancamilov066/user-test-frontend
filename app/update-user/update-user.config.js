routes.$inject = ['$stateProvider'];

export default function routes($stateProvider){
    $stateProvider
        .state('UpdateUserModule', {
            url: '/update-user',
            controller: 'UpdateUserController',
            template: require('./update-user.html')
        })
}