import angular from 'angular';
import uirouter, { fnToString } from 'angular-ui-router';
import routes from './update-user.config';
import UsersService from '../services/users.service';

var updateUserModule = angular
    .module('UsersApp.UpdateUserModule', [uirouter])
    .config(routes)
    .service('UsersService', ['$http', UsersService])
    .controller('UpdateUserController', ['$scope', 'UsersService', function($scope, UsersService){

        $scope.user = undefined;
        $scope.searchError = undefined;
        $scope.error = undefined;
        $scope.success = undefined;
        $scope.email = ''

        $scope.queryUser = function(){
            restartAll();
            UsersService.findUserByEmail($scope.email)
                .then(response => {
                    $scope.user = response.data.data;
                })
                .catch(error => {
                    if (error.status === 404) {
                        $scope.searchError = 'No encontramos ningun usuario';
                        return;
                    }
                    if (error.status === 500) {
                        $scope.searchError = 'Error interno, intenta luego';
                        return;
                    }
                })
        }

        $scope.update = function(){
            restartUpdate()
            UsersService.updateUser($scope.email, $scope.user)
                .then(response => {
                    console.log(response);
                    $scope.success = 'Usuario actualizado con exito'
                })
                .catch(error => {
                    console.log(error);
                    if (error.status === 404) {
                        $scope.error = 'No encontramos ningun usuario';
                        return;
                    }
                    if (error.status === 409) {
                        $scope.error = 'No puedes usar este correo, ya alguien lo tiene';
                        return;
                    }
                    if (error.status === 500) {
                        $scope.error = 'Error interno, intenta luego';
                        return;
                    }
                });
        }

        function restartUpdate(){
            $scope.error = undefined;
            $scope.success = undefined;
        }

        function restartAll(){
            $scope.error = undefined;
            $scope.user = undefined;
            $scope.searchError = undefined;
            $scope.success = undefined;
        }
        
    }]);

export default updateUserModule.name;