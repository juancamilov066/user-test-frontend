import { findByEmail, deleteUser, CREATE_USER, updateUser } from './Urls';

export default class UsersService {

    constructor($http) {
        this.$http = $http;
    }

    findUserByEmail(email) {
        return this.$http.get(findByEmail(email));
    }

    createUser(user) {
        return this.$http.post(CREATE_USER, user);
    }

    deleteUser(email) {
        return this.$http.delete(deleteUser(email));
    }

    updateUser(email, user) {
        return this.$http.put(updateUser(email), user);
    }

}