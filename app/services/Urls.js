const BASE_URL = 'http://localhost/users-test-backend/public/api';

export const CREATE_USER = `${BASE_URL}/user`;

export function updateUser(email){
    return `${BASE_URL}/user/${email}`;
}

export function findByEmail(email){
    return `${BASE_URL}/user/${email}`;
}

export function deleteUser(email){
    return `${BASE_URL}/user/${email}`;
}