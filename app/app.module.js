import angular from 'angular';
import uirouter from 'angular-ui-router';
import homeModule from './home/home.module';
import deleteUserModule from './delete-user/delete-user.module';
import createUserModule from './create-user/create-user.module';
import queryUserModule from './query-user/query-user.module';
import updateUserModule from './update-user/update-user.module';
import routes from './app.routes';

var app = angular
    .module('UsersApp', [
        uirouter,
        homeModule, 
        deleteUserModule,
        createUserModule,
        queryUserModule,
        updateUserModule
    ])
    .config(routes);