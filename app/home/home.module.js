import angular from 'angular';
import routes from './home.config';
import uirouter from 'angular-ui-router'
var headIcon = require('../../assets/images/user.svg');

var homeModule = angular
    .module('UsersApp.HomeModule', [uirouter])
    .config(routes)
    .controller('HomeController', ['$scope', '$state', function($scope, $state){

        $scope.headIcon = headIcon;

        $scope.createUser = function(){
            $state.go('CreateUserModule');
        }
        $scope.queryUser = function(){
            $state.go('QueryUserModule');
        }
        $scope.deleteUser = function(e){
            $state.go('DeleteUserModule');
        }
        $scope.updateUser = function(e){
            $state.go('UpdateUserModule');
        }
    }]);

export default homeModule.name;