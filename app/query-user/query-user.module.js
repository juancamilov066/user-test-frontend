import angular from 'angular';
import uirouter from 'angular-ui-router';
import routes from './query-user.config';
import UsersService from '../services/users.service';
import { validateEmail } from '../utils/Utils';

var queryUserModule = angular
    .module('UsersApp.QueryUserModule', [uirouter])
    .config(routes)
    .service('UsersService', ['$http', UsersService])
    .controller('QueryUserController', ['$scope', 'UsersService', function($scope, UsersService){

        $scope.email = '';
        $scope.error = undefined;
        $scope.user = undefined;

        $scope.queryUser = function() {
            restart();
            UsersService.findUserByEmail($scope.email)
                .then(response => {
                    $scope.user = response.data.data;
                })
                .catch(error => {
                    if (error.status === 404) {
                        $scope.error = 'No encontramos ningun usuario';
                        return;
                    }
                    if (error.status === 500) {
                        $scope.error = 'Error interno, intenta luego';
                        return;
                    }
                })
            
        }

        function restart(){
            $scope.error = undefined;
            $scope.user = undefined;
        }

    }]);

export default queryUserModule.name;