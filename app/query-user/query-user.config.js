routes.$inject = ['$stateProvider'];

export default function routes($stateProvider){
    $stateProvider
        .state('QueryUserModule', {
            url: '/query-user',
            controller: 'QueryUserController',
            template: require('./query-user.html')
        });
}