const path = require('path');
module.exports = {
    context: __dirname + '/app',
    entry: './app.module.js',
    output: {
        path: __dirname + "/app",
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            { test: /\.html$/, loader: "html-loader" },
            { test: /\.css$/, loader: "style!css" },
            { test: /\.svg$/, loader: 'svg-loader' }
        ]
    }
}